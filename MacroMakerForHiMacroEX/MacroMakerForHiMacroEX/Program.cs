﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace MacroMakerForHiMacroEX
{
    class Program
    {
        static string[] sourceFilePath;
        static string[] sourceFileName;
        static string[] sourceFileTitle;
        static int tabsLeftMargin = 10;
        static int tabsRightMargin = 140;
        static int tabWidth = 205;
        static int originTabWidth = tabWidth;
        static int overlapWidth = 12;
        static int windowWidth = 960;
        static int yPosition = 30;
        static string smallDelay = "1000";
        static int tabNum = 0;
        static int groupNum;
        static int totalCycleSec;
        static int[] groupSize;
        static List<string> title = new List<string>();
        static List<string[]> line = new List<string[]>();
        static string newFileName = "";
        static StreamWriter newFile;

        static void Main(string[] args)
        {
            ReadConfig();
            GetSourceFiles();
            ReadFileAndGetTitle();
            CulculateTabWidth();
            AskGroup();
            AskCycle();
            FinalCheck();
            Console.ReadKey();
        }
        static void ReadConfig()
        {
            try
            {
                string[] configs = File.ReadAllLines(@"config.txt", Encoding.GetEncoding("shift_jis"));
                SetData(configs);
                Console.WriteLine("Config file found!");
            }
            catch
            {
                Console.WriteLine("Couldn't find config file...");
            }
            finally
            {
                Console.WriteLine();
            }
        }
        static void SetData(string[] configs)
        {
            for (int i = 0; i < configs.Length; i++)
            {
                string[] currentRows = configs[i].Split(' ');
                if (currentRows.Length > 1)
                {
                    int value;
                    if (int.TryParse(currentRows[currentRows.Length - 1], out value))
                    {
                        switch (currentRows[0])
                        {
                            case "tabsLeftMargin":
                                tabsLeftMargin = value;
                                break;
                            case "tabsRightMargin":
                                tabsRightMargin = value;
                                break;
                            case "tabWidth":
                                tabWidth = value;
                                originTabWidth = tabWidth;
                                break;
                            case "overlapWidth":
                                overlapWidth = value;
                                break;
                            case "windowWidth":
                                windowWidth = value;
                                break;
                            case "yPosition":
                                yPosition = value;
                                break;
                            case "smallDelay":
                                smallDelay = value.ToString();
                                break;
                        }
                    }
                }
            }
        }
        static void GetSourceFiles()
        {
            sourceFilePath = Directory.GetFiles(@"sources\");
            sourceFileName = new string[sourceFilePath.Length];
            sourceFileTitle = new string[sourceFilePath.Length];
            for (int i = 0; i < sourceFileName.Length; i++)
            {
                sourceFileName[i] = Path.GetFileNameWithoutExtension("@" + sourceFilePath[i]);
                string[] currentSource = sourceFileName[i].Split('_');
                if (currentSource.Length > 1)
                {
                    sourceFileTitle[i] = currentSource[1];
                }
                else
                {
                    sourceFileTitle[i] = "";
                }
            }
        }
        static void ReadFileAndGetTitle()
        {
            title = new List<string>();
            line = new List<string[]>();
            string choise = "start";
            while (choise != "end")
            {
                WriteSourceFileList();
                Console.Write("Which file? :");
                choise = Console.ReadLine();
                AskFileNumAndTitle(choise);
            }
        }
        static void WriteSourceFileList()
        {
            for (int i = 0; i < sourceFilePath.Length; i++)
            {
                Console.WriteLine("[" + i.ToString() + "]" + sourceFileName[i]);
            }
            Console.WriteLine("[end]end");
            Console.WriteLine("[del]delete last file");
            Console.WriteLine();
            Console.Write("[");
            WriteTitle();
            Console.Write("]");
            Console.WriteLine();
            Console.WriteLine();
        }
        static void WriteTitle()
        {
            Console.Write("title: ");
            for (int i = 0; i < title.Count; i++)
            {
                Console.Write(title[i]);
                if (i != title.Count - 1)
                {
                    Console.Write(",");
                }
            }
        }
        static void AskFileNumAndTitle(string choise)
        {
            int choiseNum = 0;
            Console.WriteLine();
            if (choise == "del" && tabNum > 0)
            {
                DeleteFileAndTitle();
            }
            else if (int.TryParse(choise, out choiseNum))
            {
                AddFileAndTitle(choiseNum);
            }
        }
        static void DeleteFileAndTitle()
        {
            line.RemoveAt(line.Count - 1);
            title.RemoveAt(title.Count - 1);
            tabNum--;
        }
        static void AddFileAndTitle(int choiseNum)
        {
            if (choiseNum >= 0 && choiseNum < sourceFilePath.Length)
            {
                line.Add(File.ReadAllLines(sourceFilePath[choiseNum], Encoding.GetEncoding("shift_jis")));
                string _title = "";
                while (_title == "")
                {
                    if (sourceFileTitle[choiseNum] == "")
                    {
                        Console.Write("Title? :");
                        _title = Console.ReadLine();
                    }
                    else
                    {
                        _title = sourceFileTitle[choiseNum];
                    }
                }
                title.Add(_title);
                tabNum++;
            }
        }
        static void CulculateTabWidth()
        {
            tabWidth = originTabWidth;
            if (tabsLeftMargin + (tabWidth - overlapWidth) * tabNum + overlapWidth > windowWidth - tabsRightMargin)
            {
                tabWidth = (windowWidth - (tabsLeftMargin + tabsRightMargin + overlapWidth)) / tabNum + overlapWidth;
                Console.WriteLine("TabWidth is {0}.", tabWidth);
                Console.WriteLine();
            }
        }
        static void AskGroup()
        {
            string choise = "x";
            while (choise != "y")
            {
                choise = "x";
                AskGroupNum();
                AskGroupSize();
                choise = GroupCheck(choise);
            }
        }
        static void AskGroupNum()
        {
            while (true)
            {
                Console.Write("groupNum? :");
                if (int.TryParse(Console.ReadLine(), out groupNum) && groupNum <= tabNum)
                {
                    groupSize = new int[groupNum];
                    break;
                }
            }
        }
        static void AskGroupSize()
        {
            int currentTotal = 0;
            for (int i = 0; i < groupNum; i++)
            {
                while (true)
                {
                    Console.Write("{0} groupSize? :", i);
                    if (int.TryParse(Console.ReadLine(), out groupSize[i]))
                    {
                        if (currentTotal + groupSize[i] <= tabNum)
                        {
                            currentTotal += groupSize[i];
                            break;
                        }
                    }
                }
            }
        }
        static string GroupCheck(string choise)
        {
            Console.WriteLine();
            WriteGroup();
            Console.WriteLine();
            while (!(choise == "y" || choise == "n"))
            {
                Console.Write("OK?(y/n) :");
                choise = Console.ReadLine();
            }
            Console.WriteLine();
            return choise;
        }
        static void WriteGroup()
        {
            Console.Write("groupSize: ");
            for (int i = 0; i < groupNum; i++)
            {
                Console.Write(groupSize[i]);
                if (i != groupNum - 1)
                {
                    Console.Write(",");
                }
            }
        }
        static void AskCycle()
        {
            while (true)
            {
                Console.Write("totalCycleSec? :");
                if (int.TryParse(Console.ReadLine(), out totalCycleSec))
                {
                    Console.WriteLine();
                    break;
                }
            }
        }
        static void FinalCheck()
        {
            WriteAllInfo();
            Console.WriteLine();
            string choise = "x";
            while (!(choise == "1" || choise == "2" || choise == "3" || choise == "end"))
            {
                Console.Write("Choise? :");
                choise = Console.ReadLine();
            }
            switch (choise)
            {
                case "1":
                    ReadFileAndGetTitle();
                    CulculateTabWidth();
                    FinalCheck();
                    break;
                case "2":
                    AskGroup();
                    FinalCheck();
                    break;
                case "3":
                    AskCycle();
                    FinalCheck();
                    break;
                case "end":
                    CreateNewFile();
                    break;
            }
        }
        static void WriteAllInfo()
        {
            Console.Write("[1]");
            WriteTitle();
            Console.WriteLine();
            Console.Write("[2]");
            WriteGroup();
            Console.WriteLine();
            Console.Write("[3]");
            Console.WriteLine("totalCycleSec: " + totalCycleSec.ToString());
            Console.WriteLine("[end]end editing and create file");
        }
        static void CreateNewFile()
        {
            CreateFileName();
            newFile = new StreamWriter(newFileName + ".txt", false, Encoding.GetEncoding("shift_jis"));
            WriteIntoFile();
        }
        static void CreateFileName()
        {
            for (int i = 0; i < title.Count; i++)
            {
                newFileName += title[i];
                if (i != title.Count - 1)
                {
                    newFileName += ",";
                }
            }
        }
        static void WriteIntoFile()
        {
            int center;
            string mousePosText;
            int toNextGroup = groupSize[0];
            int currentGroupNum = 0;
            string bigDelay = (totalCycleSec * 1000 / groupNum).ToString();
            for (int i = 0; i < line.Count; i++)
            {
                center = tabsLeftMargin + (tabWidth - overlapWidth) * i + tabWidth / 2;
                mousePosText = "(" + center + "," + yPosition + ")";
                newFile.WriteLine("LMouCl Down " + mousePosText);
                newFile.WriteLine("LMouCl Up " + mousePosText);
                for (int j = 0; j < line[i].Length; j++)
                {
                    newFile.WriteLine(line[i][j]);
                }
                if(i == toNextGroup - 1)
                {
                    newFile.WriteLine(bigDelay);
                    currentGroupNum++;
                    if(currentGroupNum < groupNum)
                    {
                        toNextGroup += groupSize[currentGroupNum];
                    }
                }
                else
                {
                    newFile.WriteLine(smallDelay);
                }
            }
            newFile.Close();
        }
    }
}