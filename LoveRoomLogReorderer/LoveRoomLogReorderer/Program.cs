﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace LoveRoomLogReorderer
{
    class Program
    {
        static string fileName;
        static string[] line;
        static string newFileName;
        static StreamWriter newFile;

        static void Main(string[] args)
        {
            AskAndReadFile();
            CreateNewFile();
            WriteIntoFile();
        }
        static void AskAndReadFile()
        {
            bool isLooping = true;
            while (isLooping)
            {
                isLooping = false;
                Console.Write("FileName? :");
                fileName = Console.ReadLine();
                try
                {
                    line = File.ReadAllLines(fileName + ".txt", Encoding.GetEncoding("shift_jis"));
                    Console.WriteLine("FileRead!");
                    Console.WriteLine();
                }
                catch
                {
                    isLooping = true;
                }
            }
        }
        static void CreateNewFile()
        {
            newFileName = fileName + "_reordered.txt";
            newFile = new StreamWriter(newFileName, false, Encoding.GetEncoding("shift_jis"));
        }
        static void WriteIntoFile()
        {
            for(int i = line.Length - 1; i >= 0; i--)
            {
                newFile.WriteLine(line[i]);
                newFile.WriteLine();
            }
            newFile.Close();
        }
        static void WriteArray()
        {
            for(int i = 0; i < 5; i++)
            {
                Console.WriteLine(line[i]);
            }
        }
    }
}
